"""pnl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from sparkode import views
from django.views.generic.base import TemplateView
from django.conf.urls import url

urlpatterns = [
  path('admin/', admin.site.urls),
    path('',views.home,name='home'),
    url(r'^login/$',views.login),
    #url(r'^principal/$', views.principal),
    url(r'^logout/$',views.logout),
    url(r'^Registro/$',views.Registro),
    url(r'^Test/$',views.Test),
    url(r'^Evaluacion/(?P<tema>.+)/(?P<subtema>\d)/(?P<tipo>.+)/(?P<numero>\d)/$',views.Evaluacion),
    url(r'^tema/(?P<tema>\d)/(?P<subtema>\d)/(?P<pagina>\d)/$',views.temas,name='temas'),
    url(r'^tema/(?P<tema>\d)/(?P<subtema>\d)/(?P<pagina>\d)/$',views.redireccionar_temas,name='r_temas'),
    url(r'^ejercicio/(?P<tema>.+)/(?P<subtema>\d)/(?P<tipo>.+)/(?P<numero>\d)/$', views.ejercicios, name='ejercicios'),
    url(r'^ejercicio/(?P<tema>.+)/(?P<subtema>\d)/(?P<tipo>.+)/(?P<numero>\d)/$', views.redireccionar_ejercicios, name='r_ejercicios'),
    url(r'^temario/$',views.temario),
]
