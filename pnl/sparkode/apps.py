from django.apps import AppConfig


class SparkodeConfig(AppConfig):
    name = 'sparkode'
