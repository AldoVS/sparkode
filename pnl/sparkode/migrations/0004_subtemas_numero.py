# Generated by Django 2.2 on 2019-07-02 17:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sparkode', '0003_auto_20190627_1612'),
    ]

    operations = [
        migrations.AddField(
            model_name='subtemas',
            name='Numero',
            field=models.ManyToManyField(to='sparkode.Usuario'),
        ),
    ]
