# Generated by Django 2.2.3 on 2019-08-05 19:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sparkode', '0005_auto_20190702_1246'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ejercicio',
            old_name='IdSubTemas',
            new_name='IdSubtema',
        ),
        migrations.RenameField(
            model_name='ejercicio',
            old_name='Descripcion',
            new_name='Tipo',
        ),
        migrations.RemoveField(
            model_name='temas',
            name='Evaluacion',
        ),
        migrations.RemoveField(
            model_name='temas',
            name='IdUsuario',
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='Fecha',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='IdUsuario',
            field=models.ForeignKey(default=6, on_delete=django.db.models.deletion.CASCADE, to='sparkode.Usuario'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='Nombre',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='R1',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='R2',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='R3',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='R4',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='R5',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='ejercicio',
            name='R6',
            field=models.CharField(blank=True, max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='subtemas',
            name='NEjercicios',
            field=models.IntegerField(default=3),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='temas',
            name='NSubtemas',
            field=models.IntegerField(default=3),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='temas',
            name='Numero',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='temas',
            name='Nombre',
            field=models.CharField(max_length=60),
        ),
        migrations.CreateModel(
            name='TemaUsuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Evaluacion', models.FloatField(blank=True, null=True)),
                ('IdTema', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sparkode.Temas')),
                ('IdUsuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sparkode.Usuario')),
            ],
        ),
    ]
