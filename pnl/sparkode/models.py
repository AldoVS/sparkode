from django.db import models

class Tecnologico(models.Model):
   Nombre = models.CharField(max_length=40)
   ClavePlantel = models.CharField(max_length=20)

class Usuario(models.Model):
   NombreUsuario = models.CharField(max_length=25)
   Nombre = models.CharField(max_length=20)
   ApellidoPaterno = models.CharField(max_length=30)
   ApellidoMaterno = models.CharField(max_length=30)
   Password = models.CharField(max_length=15)
   Email = models.EmailField()
   FechaNacimiento = models.DateField()
   Nivel = models.IntegerField()
   Progreso = models.IntegerField()
   IdTecnologico = models.ForeignKey(Tecnologico, on_delete=models.CASCADE)
  
class Aprendizaje(models.Model):
   Visual = models.FloatField()
   Auditivo = models.FloatField()
   Kinestesico = models.FloatField()
   IdUsuario = models.OneToOneField(Usuario, on_delete=models.CASCADE)

class Temas(models.Model):
   Nombre = models.CharField(max_length=60)
   Numero=models.IntegerField()
   NSubtemas=models.IntegerField()

class TemaUsuario(models.Model):
   IdUsuario=models.ForeignKey(Usuario, on_delete=models.CASCADE)
   IdTema=models.ForeignKey(Temas, on_delete=models.CASCADE)
   Evaluacion = models.FloatField(null= True,blank=True)

class SubTemas(models.Model):
   Nombre = models.CharField(max_length=60)
   Numero=models.IntegerField()
   NEjercicios=models.IntegerField()
   IdTemas = models.ForeignKey(Temas, on_delete=models.CASCADE)

class Ejercicio(models.Model):
   IdUsuario=models.ForeignKey(Usuario, on_delete=models.CASCADE)
   IdSubtema=models.ForeignKey(SubTemas, on_delete=models.CASCADE)
   Nombre=models.CharField(max_length=20,null=True,blank=True)
   Tipo = models.CharField(max_length=25)
   Evaluacion = models.FloatField(null = True)
   Fecha=models.DateField(null=True,blank=True)
   R1=models.CharField(max_length=40,null=True,blank=True)
   R2=models.CharField(max_length=40,null=True,blank=True)
   R3=models.CharField(max_length=40,null=True,blank=True)
   R4=models.CharField(max_length=40,null=True,blank=True)
   R5=models.CharField(max_length=40,null=True,blank=True)
   R6=models.CharField(max_length=40,null=True,blank=True)

class EjercicioR(models.Model):
   IdTem = models.ForeignKey(Temas, on_delete=models.CASCADE)
   IdSub = models.ForeignKey(SubTemas, on_delete=models.CASCADE)
   Descripcion = models.CharField(max_length=20,null=True,blank=True)
   NEjer = models.IntegerField()
   Tipo = models.CharField(max_length=20)
   RP1 = models.CharField(max_length=40,null=True,blank=True)
   RP2 = models.CharField(max_length=40,null=True,blank=True)
   RP3 = models.CharField(max_length=40,null=True,blank=True)
   RP4 = models.CharField(max_length=40,null=True,blank=True)
   RP5 = models.CharField(max_length=40,null=True,blank=True)
   RP6 = models.CharField(max_length=40,null=True,blank=True)
   RP7 = models.CharField(max_length=40,null=True,blank=True)
   RP8 = models.CharField(max_length=40,null=True,blank=True)
   RP9 = models.CharField(max_length=40,null=True,blank=True)
   RP10 = models.CharField(max_length=40,null=True,blank=True)
