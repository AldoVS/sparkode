import json
from django.shortcuts import render, render_to_response
from django.db.models import Q
from django.http  import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from sparkode.models import Usuario, Aprendizaje, Temas, SubTemas, Ejercicio, TemaUsuario, EjercicioR
from django.views import View
#from braces.views import JSONResponseMixin, LoginRequiredMixin
from django.contrib import messages

# Create your views here.
def home(request):
	if 'member_id' in request.session:
		return render_to_response("principal.html")

	return HttpResponseRedirect('/login/')

def login(request):
  errors = []
  if request.method == 'POST':
      if not request.POST.get('usuario', ''):
          errors.append('Por favor introduce el asunto.')
      if not request.POST.get('password', ''):
          errors.append('Por favor introduce un mensaje.')
      if not errors:
          try:
             m = Usuario.objects.get(NombreUsuario=request.POST['usuario'])
          except Usuario.DoesNotExist:
             errors.append('El usuario no existe.')
          else:
             if m.Password == request.POST['password']:
                request.session['member_id'] = m.id
                request.session['member_Nivel'] = m.Nivel
                request.session['member_Progreso'] = m.Progreso
                if m.Nivel == 0 and m.Progreso == 0:
                    return HttpResponseRedirect('/Test/')
                return HttpResponseRedirect(reverse('home'))
             errors.append('Tu contraseña no coincide con el Usuario.')
  if 'member_id' in request.session:
    return HttpResponseRedirect(reverse('home'))

  return render(request,'login.html', {'errors': errors})

def logout(request):
  try:
     del request.session['member_id']
  except KeyError:
   pass
  return HttpResponseRedirect('/login/')
  #return HttpResponse("Haz salido de la sesión")

def Registro(request):
  errors = []
  if 'member_id' in request.session:
        return HttpResponseRedirect(reverse('home'))
  if request.method == 'POST':
      if not request.POST.get('Nombre', ''):
          errors.append('Por favor introduce un Nombre.')
      if not request.POST.get('ApellidoP', ''):
          errors.append('Por favor introduce un Apellido Paterno.')
      if not request.POST.get('ApellidoM', ''):
          errors.append('Por favor introduce un Apellido Materno.')
      if not request.POST.get('Edad', ''):
          errors.append('Por favor introduce una Edad.')
      if not request.POST.get('Usuario', ''):
          errors.append('Por favor introduce un Usuario.')
      if not request.POST.get('Password', ''):
          errors.append('Por favor introduce una Contraseña.')
      if not request.POST.get('Password2', ''):
          errors.append('Por favor introduce una Contraseña en repetir Contraseña .')
      if not request.POST.get('Email', ''):
          errors.append('Por favor introduce un Email.')
      if not errors:
          if request.POST['Password'] == request.POST['Password2']:
              
              try:
                 Usuario.objects.get(NombreUsuario=request.POST['Usuario'])
              except Usuario.DoesNotExist:
                    try:
                       Usuario.objects.get(Email=request.POST['Email'])
                    except  Usuario.DoesNotExist:
                         insert= Usuario(NombreUsuario=request.POST['Usuario'],Nombre=request.POST['Nombre'],ApellidoPaterno=request.POST['ApellidoP'], ApellidoMaterno=request.POST['ApellidoM'],Password=request.POST['Password'],Email=request.POST['Email'],FechaNacimiento=request.POST['Edad'],Nivel=0, Progreso=0,IdTecnologico_id=1)
                         
                         insert.save()
                         
                         return HttpResponseRedirect('/login/')
                         
                    else:
                        errors.append('El Email ya exite.')
              else:
                  errors.append('El usuario ya exite.')

          if request.POST['Password'] != request.POST['Password2']:
              errors.append('Las contraseñas no coinciden.')

  return render(request,'signup.html', {'errors': errors})

def Test (request):
  errors = []

  if 'member_id' in request.session:
    if request.session['member_Nivel'] > 0 and request.session['member_Progreso'] > 0:
      return HttpResponseRedirect(reverse('home'))
    if request.method == 'POST':
          
        Pa=int(request.POST['p2'])+int(request.POST['p5'])+int(request.POST['p12'])+int(request.POST['p14'])+int(request.POST['p15'])+int(request.POST['p17'])
        Pv=int(request.POST['p1'])+int(request.POST['p3'])+int(request.POST['p6'])+int(request.POST['p9'])+int(request.POST['p10'])+int(request.POST['p11'])
        Pk=int(request.POST['p4'])+int(request.POST['p7'])+int(request.POST['p8'])+int(request.POST['p13'])+int(request.POST['p16'])+int(request.POST['p18'])
        Pt=Pa+Pv+Pk
        EvAu= round((Pa/Pt)*100,4)
        EvVi= round((Pv/Pt)*100,4)
        EvKi= round((Pk/Pt)*100,4)
        
        try:
          Aprendizaje.objects.get(IdUsuario_id=request.session['member_id'])
        except Aprendizaje.DoesNotExist:
          insert= Aprendizaje(Visual=EvVi,Auditivo=EvAu,Kinestesico=EvKi,IdUsuario_id=request.session['member_id'])
          insert.save()
          Usuario.objects.filter(id=request.session['member_id']).update(Progreso=1)
          Usuario.objects.filter(id=request.session['member_id']).update(Nivel=1)
          request.session['member_Nivel'] = 1
          request.session['member_Progreso'] = 1
          return render(request,'Resultados.html',{'Auditivo':EvAu,'Visual':EvVi ,'Kinestesico':EvKi})
        else:
          return HttpResponse("Consulte al Administrador")
    return render(request,'Test.html', {'errors': errors})

  return HttpResponseRedirect('/login/')

def Evaluacion(request, tema, subtema, tipo, numero):
  errors=[]
  Direccion='Unidades/Ejercicios/%s-%d-%s-%d.html'%(tema, int(subtema), tipo, int(numero))
  Evaluacion=0
  Bandera1=0
  Resultado=0
  subtema='%d.%d'%(request.session['member_Nivel'],request.session['member_Progreso'])
  if 'member_id' in request.session:
    if request.method == 'POST':
      for key, value in request.POST.items():
        if key != 'csrfmiddlewaretoken':
          Numero= float(value)
          Bandera1+=1
          Evaluacion +=Numero
      Resultado=Evaluacion*1
      round(Resultado)

      insert= Ejercicio(Tipo='%s'%(tipo),
        IdUsuario_id=request.session['member_id'],
        Evaluacion=Resultado,
        IdSubtema=SubTemas.objects.get(
          Q(IdTemas=Temas.objects.get(Numero=request.session['member_Nivel'])),
          Q(Numero=request.session['member_Progreso']))
        )
      insert.save()
      messages.info(request,'Tu puntuaje es: %d'%Resultado)
      return render(request,Direccion,{'resultado':Resultado})
    return render(request, Direccion,{'errors':errors})
  return HttpResponseRedirect('/login/')
 

def temas(request, tema, subtema, pagina):
	
	return render(request, 'Unidades/Temas/%d-%d-%d.html'%(int(tema), int(subtema), int(pagina)))

def redireccionar_temas(request,tema,subtema,pagina):

	return HttpResponseRedirect(reverse('/temas/',args(tema,subtema,pagina,)))


def ejercicios(request, tema, subtema, tipo, numero):
	return render(request,'Unidades/Ejercicios/%s-%d-%s-%d.html'%(tema, int(subtema), tipo, int(numero)))

def redireccionar_ejercicios(request,tema,subtema,tipo,numero):

	return HttpResponseRedirect(reverse('/ejercicios/',args(tema,subtema,tipo,numero,)))

def temario(request):
	return render(request, 'Temario.html')